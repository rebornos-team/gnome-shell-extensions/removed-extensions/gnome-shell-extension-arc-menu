# gnome-shell-extension-arc-menu

**Modified by Rafael to work with master zip file.**

A Dynamic / Traditional / Modern Extension Menu for GNOME

GitLab Page: https://gitlab.com/arcmenu-team/Arc-Menu

New GitLab Page: https://gitlab.com/arcmenu/ArcMenu

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/gnome-shell-extensions/gnome-shell-extension-arc-menu.git
```

